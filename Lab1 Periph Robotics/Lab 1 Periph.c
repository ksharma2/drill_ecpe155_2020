#include <stdint.h>
#include <stdbool.h>

#include "inc/tm4c123gh6pm.h"
#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"

void main (void){

    //Enable the GPIOF peripheral
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    // Wait for the GPIOF module to be ready.
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF)) { }

    // Make PF1 output pins and enable them
    GPIODirModeSet(GPIO_PORTF_BASE, GPIO_PIN_1, GPIO_DIR_MODE_OUT);
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1);

     while (true){
         // Step through at a rate that can be observed
         for (uint32_t i = 0; i < 1000000; ++i)
             GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, 0); //Turn off LED

         for (uint32_t i = 0; i < 1000000; ++i)
             GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, ~0); //Turn on LED
     }
}
