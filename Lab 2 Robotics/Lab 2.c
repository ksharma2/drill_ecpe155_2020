#include <stdint.h>
#include <stdbool.h>

#include "inc/tm4c123gh6pm.h"
#include "driverlib/pwm.h"
#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"


int main(void)
{
    //Enable the GPIO peripherals
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    // Wait for the GPIO modules to be ready.
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB)) { }
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF)) { }

    //Configures pin(s) for use by the PWM peripheral
    GPIOPinTypePWM(GPIO_PORTB_BASE ,GPIO_PIN_6 | GPIO_PIN_7);

    GPIOPinConfigure(GPIO_PB6_M0PWM0);

    //Enable the PWM0 peripheral. Addr 0x40028000
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);

    // Wait for the PWM0 module to be ready. //
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM0)) { }

    // Configure the PWM generator for count down mode with immediate updates to the parameters.
    PWMGenConfigure(PWM0_BASE, PWM_GEN_0, PWM_GEN_MODE_DOWN |PWM_GEN_MODE_NO_SYNC);

    // Set the period. For a 50 KHz frequency, the period = 1/50,000, or 20 microseconds. For a 20 MHz clock,
    //this translates to 400 clock ticks. Use this value to set the period.
    PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0, 400);

    // Set the pulse width of PWM0 for a 25% duty cycle.
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_0, 100);

    // Set the pulse width of PWM1 for a 75% duty cycle.
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, 300);

    // Start the timers in generator 0.
    PWMGenEnable(PWM0_BASE, PWM_GEN_0);

    // Enable the outputs.
    PWMOutputState(PWM0_BASE, (PWM_OUT_0_BIT | PWM_OUT_1_BIT), true);
}
