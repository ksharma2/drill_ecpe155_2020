// Definitions for datatypes used in labs.
#include <stdint.h>
#include <stdbool.h>

// SYSCTL and GPIO definitions.
#include "sysctl.h"
#include "gpio.h"
#include "inc/tm4c123gh6pm.h"

void main (void){
    // Turn on run clock gate control for Port F.
     SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTF;

     // Make PF1, PF2, PF3 output pins and enable them
     GPIO_PORTF[GPIO_DIR] |= GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3;
     GPIO_PORTF[GPIO_DEN] |= GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3;

     while (true){
         // Step through at a rate that can be observed
         for (uint32_t i = 0; i < 1000000; ++i)
             GPIO_PORTF[GPIO_DATA] &= ~GPIO_PIN_1; //Turn off LED
         for (uint32_t i = 0; i < 1000000; ++i)
             GPIO_PORTF[GPIO_DATA] |= GPIO_PIN_1; //Turn on LED
     }
}

