#ifndef _SYSCTL_H
#define _SYSCTL_H

#include <stdint.h>

// Peripheral base addresses.
#define SYSCTL          (((volatile uint32_t *)0x400fe000))

// Peripheral register offsets and special fields.
enum {
  SYSCTL_RCGCGPIO =       (0x608 >> 2),
#define   SYSCTL_RCGCGPIO_PORTF (1 << 5)
};

#endif // _SYSCTL_H
